from .base_settings import *

import os
import secrets
import logging


logger = logging.getLogger(__name__)


# SECURITY WARNING: keep the secret key used in production secret!
# if no key is set then the default is a random key with 64 bytes
# str Base64 encoded
SECRET_KEY = os.environ.get('FLIPSTARTER_DIRECTORY_SECRET_KEY', secrets.token_urlsafe(64))

DEBUG = False

REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = [
    'rest_framework.renderers.JSONRenderer',
]

def split_args(env_var):
    """
    Fetches a comma separated from env
    variable FLIPSTARTER_DIRECTORY_ALLOWED_HOSTS
    and then splits them into an array and strips all
    spaces from beginning and end.
    """
    hosts = os.environ.get(env_var, '')
    if hosts == '':
        return []
    return list(map(lambda h: h.strip(), hosts.split(',')))

def get_allowed_hosts():
    """
    Fetches a list of hosts comma separated from env
    variable FLIPSTARTER_DIRECTORY_ALLOWED_HOSTS
    and then splits them into an array and strips all
    spaces from beginning and end.
    """
    return split_args('FLIPSTARTER_DIRECTORY_ALLOWED_HOSTS')

ALLOWED_HOSTS = get_allowed_hosts()
logger.info(f'ALLOWED_HOSTS: {ALLOWED_HOSTS}')

def get_cors_allowed_origins():
    """
    Fetches a list of domains with protocol comma separated from env
    variable FLIPSTARTER_DIRECTORY_CORS_ALLOWED_ORIGINS
    and then splits them into an array and strips all
    spaces from beginning and end.
    Format should be "http://domain1.com,https://domain2.xyz"
    """
    return split_args('FLIPSTARTER_DIRECTORY_CORS_ALLOWED_ORIGINS')

CORS_ALLOWED_ORIGINS = get_cors_allowed_origins()
logger.info(f'CORS_ALLOWED_ORIGINS: {CORS_ALLOWED_ORIGINS}')

CORS_ALLOW_ALL_ORIGINS = False
if os.environ.get('FLIPSTARTER_DIRECTORY_CORS_ALLOW_ALL_ORIGINS', False) == 'yes':
    CORS_ALLOW_ALL_ORIGINS = True

logger.info(f'CORS_ALLOW_ALL_ORIGINS: {CORS_ALLOW_ALL_ORIGINS}')

USQ_SQLITE = os.environ.get('FLIPSTARTER_DIRECTORY_USE_SQLITE', 'no')

if USQ_SQLITE == 'yes':
    SQLITE = os.environ.get('FLIPSTARTER_DIRECTORY_SQLITE_PATH', BASE_DIR / 'db.sqlite3')
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': SQLITE,
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': os.environ.get('FLIPSTARTER_DIRECTORY_DB_NAME'),
            'USER': os.environ.get('FLIPSTARTER_DIRECTORY_DB_USER'),
            'PASSWORD': os.environ.get('FLIPSTARTER_DIRECTORY_DB_PASSWORD'),
            'HOST': os.environ.get('FLIPSTARTER_DIRECTORY_DB_HOST'),
            'PORT': os.environ.get('FLIPSTARTER_DIRECTORY_DB_PORT'),
        }
    }

STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

USE_X_FORWARDED_HOST = True
if os.environ.get('FLIPSTARTER_DIRECTORY_USE_X_FORWARDED_HOST', False) == 'no':
    USE_X_FORWARDED_HOST = False
else:
    logger.warning(f'USE_X_FORWARDED_HOST: {USE_X_FORWARDED_HOST} - make sure you are running behind a reverse proxy.')
