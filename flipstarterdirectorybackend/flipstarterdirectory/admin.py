import json
from django.conf import settings

from django.contrib import admin
from django.contrib.admin.utils import unquote
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.template.response import SimpleTemplateResponse, TemplateResponse
from django.urls import path
from django.utils.translation import gettext as _

from . import models, forms


# Register your models here.
class FlipstarterScreenshotInlineAdmin(admin.TabularInline):
    model = models.FlipstarterScreenshot
    extra = 0

class FlipstarterArchiveInlineAdmin(admin.TabularInline):
    model = models.FlipstarterArchive
    extra = 0


class FlipstarterAnnouncementInlineAdmin(admin.TabularInline):
    model = models.FlipstarterAnnouncement
    extra = 0


class FlipstarterAdmin(admin.ModelAdmin):
    inlines = [
        FlipstarterScreenshotInlineAdmin,
        FlipstarterArchiveInlineAdmin,
        FlipstarterAnnouncementInlineAdmin
    ]
    search_fields = ['title', 'description']
    list_filter = ('approved', 'status', 'categories')

    def get_urls(self):
        urls = super().get_urls()
        info = self.model._meta.app_label, self.model._meta.model_name
        my_urls = [
            path(
                'import-json/',
                self.admin_site.admin_view(self.import_from_json_view),
                name='%s_%s_import_from_json' % info
            ),
            path(
                'import-url/',
                self.admin_site.admin_view(self.import_from_url_view),
                name='%s_%s_import_from_url' % info
            ),
            path(
                '<path:object_id>/change/refresh/',
                self.admin_site.admin_view(self.refresh_flipstarter_view),
                name='%s_%s_refresh' % info
            ),
            path(
                'refresh-all',
                self.admin_site.admin_view(self.refresh_all_flipstarters_view),
                name='%s_%s_refresh_all' % info
            ),
            path(
                '<path:object_id>/change/screenshot/',
                self.admin_site.admin_view(self.screenshot_flipstarter_view),
                name='%s_%s_screenshot' % info
            ),
        ]
        return my_urls + urls

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        # add new variable to template render context so we can know if we need to add the screenshot
        # button the the change form page
        context['can_take_screenshot'] = False if settings.FLIPSTARTER_DIRECTORY_CHROME_REMOTE is None else True
        return super().render_change_form(request, context, add, change, form_url, obj)

    @admin.options.csrf_protect_m
    def import_from_json_view(self, request):
        """
        Imports from a JSON file
        format see here:
        https://gitlab.com/dagurval/flipstarters/-/blob/master/src/flipstarters.json
        """
        from django.contrib.admin.views.main import ERROR_FLAG
        if not self.has_change_permission(request):
            raise PermissionDenied

        show_done = False
        imported = ()
        if request.method == 'POST':
            json_file = request.FILES.get('flipstarter_json', None)
            if json_file is not None:
                imported = models.Flipstarter.objects.import_from_json(json.load(json_file))
                show_done = True

        try:
            cl = self.get_changelist_instance(request)
        except admin.options.IncorrectLookupParameters:
            # Wacky lookup parameters were given, so redirect to the main
            # changelist page, without parameters, and pass an 'invalid=1'
            # parameter via the query string. If wacky parameters were given
            # and the 'invalid=1' parameter was already in the query string,
            # something is screwed up with the database, so display an error
            # page.
            if ERROR_FLAG in request.GET:
                return SimpleTemplateResponse('admin/invalid_setup.html', {
                    'title': _('Database error'),
                })
            return HttpResponseRedirect(request.path + '?' + ERROR_FLAG + '=1')

        context = dict({
                # Include common variables for rendering the admin template.
            **self.admin_site.each_context(request),
            'form': forms.FlipstarterImportJSONForm,
            'show_done': show_done,
            'imported': imported,
            'cl': cl,
        })
        return TemplateResponse(request, 'flipstarter/import.html', context)

    @admin.options.csrf_protect_m
    def import_from_url_view(self, request):
        """
        Imports from a flipstarter campaign url
        """
        from django.contrib.admin.views.main import ERROR_FLAG
        if not self.has_change_permission(request):
            raise PermissionDenied

        show_done = False
        if request.method == 'POST':
            url = request.POST.get('flipstarter_url', None)
            models.Flipstarter.objects.import_from_url(url)
            show_done = True

        try:
            cl = self.get_changelist_instance(request)
        except admin.options.IncorrectLookupParameters:
            # Wacky lookup parameters were given, so redirect to the main
            # changelist page, without parameters, and pass an 'invalid=1'
            # parameter via the query string. If wacky parameters were given
            # and the 'invalid=1' parameter was already in the query string,
            # something is screwed up with the database, so display an error
            # page.
            if ERROR_FLAG in request.GET:
                return SimpleTemplateResponse('admin/invalid_setup.html', {
                    'title': _('Database error'),
                })
            return HttpResponseRedirect(request.path + '?' + ERROR_FLAG + '=1')

        context = dict({
                # Include common variables for rendering the admin template.
            **self.admin_site.each_context(request),
            'form': forms.FlipstarterImportURLForm,
            'show_done': show_done,
            'cl': cl,
        })
        return TemplateResponse(request, 'flipstarter/import.html', context)

    @admin.options.csrf_protect_m
    def refresh_flipstarter_view(self, request, object_id):
        """
        Refresh an individual flipstarter
        uses force=True to refresh no matter what
        See the refresh_flipstarter in the model for more info
        @object_id id of the flipstarter
        """
        if not self.has_change_permission(request):
            raise PermissionDenied

        obj = self.get_object(request, unquote(object_id))
        obj.refresh_flipstarter(force=True)

        info = self.model._meta.app_label, self.model._meta.model_name
        return redirect('admin:%s_%s_change' % info, object_id=obj.pk)

    @admin.options.csrf_protect_m
    def refresh_all_flipstarters_view(self, request):
        """
        Refresh all the flipstarters that need to be refreshed
        """
        if not self.has_change_permission(request):
            raise PermissionDenied

        models.Flipstarter.objects.refresh_all()
        info = self.model._meta.app_label, self.model._meta.model_name
        return redirect('admin:%s_%s_changelist' % info)

    @admin.options.csrf_protect_m
    def screenshot_flipstarter_view(self, request, object_id):
        """
        Refresh an individual flipstarter
        uses force=True to refresh no matter what
        See the refresh_flipstarter in the model for more info
        @object_id id of the flipstarter
        """
        if not self.has_change_permission(request):
            raise PermissionDenied

        obj = self.get_object(request, unquote(object_id))
        obj.get_screenshot(force=True)

        info = self.model._meta.app_label, self.model._meta.model_name
        return redirect('admin:%s_%s_change' % info, object_id=obj.pk)


admin.site.register(models.Flipstarter, FlipstarterAdmin)


class FlipstarterCategoryAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.FlipstarterCategory, FlipstarterCategoryAdmin)
