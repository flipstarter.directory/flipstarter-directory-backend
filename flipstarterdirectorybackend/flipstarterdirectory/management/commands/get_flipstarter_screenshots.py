from django.core.management.base import BaseCommand

from flipstarterdirectory import models

from ._commandmixin import FlipstarterCommandMixin


class Command(FlipstarterCommandMixin, BaseCommand):
    help = 'Fetches flipstarter screenshots where needed.'

    def handle(self, *args, **options):
        # tuple (success, failed, got_exception)
        ret = models.Flipstarter.objects.get_screenshots()

        if self.nothing(ret):
            return

        self.print_success_message(
            'Successfully got screenshot{plural} for {count} flipstarter{plural}.',
            ret[0]
        )

        self.print_success_message(
            'Failed to get screenshot{plural} for {count} flipstarter{plural}.',
            ret[1]
        )

        self.print_success_message(
            'Got exception{plural} for {count} flipstarter{plural}.',
            ret[2]
        )
