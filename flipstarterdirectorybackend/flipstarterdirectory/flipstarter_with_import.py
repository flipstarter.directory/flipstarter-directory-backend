from socket import timeout
from threading import Thread
from urllib.error import URLError
from urllib.parse import urlparse

from . import models
from .bitcoin import bch_to_satoshi


def make_kwargs(flipstarter_data):
    """
    helper function to extract data from the flipstarter_data dict
    attempts to extract data from the dict, then tries to extract
    from the url, to update some fields.
    @flipstarter_data the json data for a particular flipstarter
                      converted to a dict
    """
    kwargs = {
        'amount': bch_to_satoshi(flipstarter_data.get('amount', 0)),
        'title': flipstarter_data.get('title', ''),
        'description': flipstarter_data.get('description', ''),
        # some entries for tx have null
        'url': flipstarter_data.get('url', ''),
        'status': flipstarter_data.get('status', models.Flipstarter.Status.RUNNING),
        'funded_tx': flipstarter_data.get('tx', '') or '',
        # imported flipstarters are assumed to already be validated
        'approved': True
    }

    # try:
    #     parsed_url = urlparse(flipstarter_data.get('url'))
    #     extracted_data_from_url = models.Flipstarter.objects.extract_data_from_campaign_url(
    #         parsed_url
    #     )
    #     extracted_data_from_url.pop('last_updated')
    # except (URLError, timeout):
    #     return kwargs

    # for k in extracted_data_from_url:
    #     # if we already had a transaction id supplied then keep it
    #     # some flipstarters reused the same domain
    #     # not even a second campaign on the same domain ie. with the #2
    #     # this avoids replacing any field for an incorrect one
    #     if k and kwargs[k] != '':
    #         continue
    #     kwargs[k] = extracted_data_from_url[k]

    return kwargs

def create_flipstarter(flipstarter_data, categories):
    """
    Create a flipstarter from json data
    This is used to import flipstarter data from a JSON file
    @flipstarter_data the json data for a particular flipstarter
                      converted to a dict
    @categories a dict with the categories already loaded, to avoid loading or
                trying to create the same category twice
    """
    flipstarter = models.Flipstarter.objects.create(
        **make_kwargs(flipstarter_data)
    )

    # (create if needed) and add categories to the flipstarter
    categories_to_add = []
    for category in flipstarter_data.get('category', []):
        if category == '':
            continue
        if category not in categories:
            raise Exception(f'Category not in category cache {category}')
        categories_to_add.append(categories[category])
    if categories_to_add:
        flipstarter.categories.add(*categories_to_add)

    # helper function to add url based entries (archive, announcement)
    def create_url_entries(model, key):
        new_entries = []
        for url in flipstarter_data.get(key, []):
            new_entries.append(model(
                flipstarter=flipstarter,
                url=url,
            ))
        model.objects.bulk_create(new_entries)
    # add archive urls
    create_url_entries(models.FlipstarterArchive, 'archive')
    # add announcement urls
    create_url_entries(models.FlipstarterAnnouncement, 'announcement')
    return True
