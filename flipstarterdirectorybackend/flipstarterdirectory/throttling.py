from rest_framework.decorators import throttle_classes
from rest_framework.throttling import AnonRateThrottle


class MinutelyAnonRateThrottle(AnonRateThrottle):
    scope = 'minute-anon'


class HourlyAnonRateThrottle(AnonRateThrottle):
    scope = 'hour-anon'


class DailyAnonRateThrottle(AnonRateThrottle):
    scope = 'day-anon'