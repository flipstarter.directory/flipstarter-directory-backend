## =================================================================
-------------------------------------------------------------------

## flipstarterdirectortybackend Alpine images
## These are unstable/untested and should not be ran in production

-------------------------------------------------------------------
## =================================================================

# Production

This is a simple set of scripts to help you get started deploying to production.
It creates three docker images.
- Nginx your django static content.
- Django app using daphne to serve it. (This image also contains
static files and media files, which includes screenshots.)
- Chrome - a version of the selenium standalone chrome running with sandbox, make
sure to use the seccomp profile for docker, or chrome won't run properly.
(See the docker-compose on how to add the seccomp profile.)

```
/flipstarterdirectorybackend/static
/flipstarterdirectorybackend/media
/flipstarterdirectorybackend/media/screenshots
```

Django expects these to be in domain/static, and domain/media.

See `Serving STATIC and MEDIA files` section

Nginx docker image is configured to proxy to daphne and serve the static content.

Set your environment variables:
```
database-variables.env
nginx-variables.env
flipstarterdirectorybackend-variables.env
```

Once that is done.

Create a virtual env and install the ../requirements.txt

Run collect static with prod_settings

```
DJANGO_SETTINGS_MODULE=flipstarterdirectorybackend.prod_settings python3 manage.py collectstatic
```

Build the images, if you use sudo to run your docker commands then append sudo
```
./build version [sudo]
```
by default this builds an image with chromium and chromewebdriver included this can get big
so to build an image with no chrome or chromedriver run CHROME=no

modify docker-compose.yml to run your image version.

```
image: "flipstarterdirectory-nginx:version"
```

```
docker-compose up -d
```

### Alpine
While there is a Dockerfile for the django app built with alpine this has not been fully tested and is as is. It might not work at all.

## Deployment

If you are deploying the django/flipstarterdirectorybackend image individually without the included docker-compose:

You will need to configure certain env variables.

`FLIPSTARTER_DIRECTORY_ALLOWED_HOSTS=`
See https://docs.djangoproject.com/en/3.1/ref/settings/#allowed-hosts

For a Postgres backed deployment (default)

`FLIPSTARTER_DIRECTORY_DB_NAME=`
`FLIPSTARTER_DIRECTORY_DB_USER=`
`FLIPSTARTER_DIRECTORY_DB_PASSWORD=`
`FLIPSTARTER_DIRECTORY_DB_HOST=`
`FLIPSTARTER_DIRECTORY_DB_PORT=`

For a Sqlite backed deployment

To enable Sqlite use `FLIPSTARTER_DIRECTORY_USE_SQLITE=yes`

Use `FLIPSTARTER_DIRECTORY_SQLITE_PATH` to set the path of where you sqlite db will live including it's name.

Example `/home/user/db.sqlite3`

Default: `BASE_DIR / 'db.sqlite3'` with BASE_DIR in the docker container being `/flipstarterdirectorybackend`

If you want to mount this as a local directory you should change the default location as it is in the main directory and mounting that as a local volume would remove all the files.

See `docker-compose-sqlite.yml` for an example.

##### Optional Enviroment Variables:

`FLIPSTARTER_DIRECTORY_CORS_ALLOWED_ORIGINS`
Lets you define the hosts that are allowed to query the API.

See https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

Format is a list comma separated. Should be the domain with protocol `http://domain.com`


`FLIPSTARTER_DIRECTORY_CORS_ALLOW_ALL_ORIGINS`
Allows any origin to query the API. Overrides the setting above to allow any origin. `=yes`

`FLIPSTARTER_DIRECTORY_CHROME_REMOTE`
Sets the location of your selenium chrome standalone.

We provide an image that runs with sandbox and seccomp, which should be safer than the selenium standalone chrome which runs with --no-sandbox

Without this screenshot taking will not be available.

`FLIPSTARTER_DIRECTORY_SCREENSHOT_THREADS`
Sets the number of threads that are used when creating screenshots.

Because with this as it uses chrome --headless to create the screenshots and that can really make your memory usage spike.

`FLIPSTARTER_DIRECTORY_USE_X_FORWARDED_HOST`
Tell Django to use the X-Forwarded-Host header instead of the host header. This is used when returning uris, with the domain included. If running behind a reverse proxy you will get the ip of the container.

### Chrome

If you deploy the chrome container you are free to use

```
flipstarterdirectory/chrome:version
```
or
```
https://hub.docker.com/r/selenium/standalone-chrome/
```

The difference is that the selenium version runs with `--no-sandbox` and our image doesn't which means it needs a seccomp profile to run properly. See the `docker-compose.yml` for an example of this.

See what are the security implications of running with `--no-sandbox`. We strongly advise to run with sandbox in this case as you are accessing untursted websites.

Make sure you give chrome access to the shared memory. See `docker-compose.yml` for an example or read the selenium docs.

## Serving STATIC and MEDIA files

For a more detailed explanation read through the Django docs https://docs.djangoproject.com/en/3.1/howto/static-files/deployment/

Django expects the static and media files to be in:
```
(http|https)://domain/static
(http|https)://domain/media
```

However Django won't serve those files. We recommend nginx, or any other webserver to serve these. See our nginx config in docker/nginx/nginx/templates for an example of serving the static files.

If you are using our images, using our docker-compose.yml is probably the easiest.

If you have your own reverse proxy set up then you can use the static files provided with the flipstarterdirectorybackend docker image.

The live in `/flipstarterdirectorybackend/static`.

You can create a directory mapping to the host of the container and then serve the files using a config similar to our provided nginx config.

```
location /static {
    alias you_folder_with_static_files;
}
```

First create the folder as docker will create as root and our image does not run as root.

Then change the ownership. Our user in the image has a uid 2000. You will probably need sudo for this.

```
mkdir -p you_folder_with_static_files
sudo chown 2000:2000 you_folder_with_static_files
```

Then in the docker-compose (or adapt to docker create/run syntax):

```
volumes:
  - /you_folder_with_static_files/:/flipstarterdirectorybackend/static
```

## After deployment

After you've deployed you need to create your django super user

You can do this by executing.

```
docker exec -ti container_name /flipstarterdirectorybackend/manage.py createsuperuser
```
