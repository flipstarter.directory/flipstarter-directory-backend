# use this script to get certificates from let's encrypt
echo "${DOMAIN}"

if [ -z "${DOMAIN}" ]
then
    echo "var DOMAIN not present";
    exit 1;
fi

if [ -z "${CERTBOT_EMAIL}" ]
then
    echo "var CERTBOT_EMAIL not present";
    exit 1;
fi

if [ -z "${DJANGO_DAPHNE_SERVER}" ]
then
    echo "var DJANGO_DAPHNE_SERVER not present";
    exit 1;
fi

ORIGINAL_TEMPLATE=/etc/nginx/templates/django.conf.template
TEMPLATE=/etc/nginx/templates/${DOMAIN}.conf.template

mv ${ORIGINAL_TEMPLATE} ${TEMPLATE}

if [ "x${SSL_CERT}" != "xno" ]
then
# run this in the background
# alpine to run with & and then reacquire with fg
# doesn't work well because of it having no tty set
# so we send it to the back but make it wait 10s
# so nginx can start
echo "sleep 10" >> /certbot.run
echo "certbot --nginx --non-interactive --agree-tos --email=${CERTBOT_EMAIL} -d ${DOMAIN}" >> /certbot.run
sh /certbot.run &
fi

/docker-entrypoint.sh nginx -g "daemon off;"

# fg %1