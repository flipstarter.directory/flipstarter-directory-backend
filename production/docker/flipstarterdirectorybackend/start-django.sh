check_error() {
    STATUS=$1
    if [ "x$STATUS" != "x0" ]
    then
        echo "exiting."
        exit 1
    fi
}
pushd .
cd /flipstarterdirectorybackend
# static files
python3.8 manage.py collectstatic --no-input
STATUS=$?
check_error $STATUS
# migrations
python3.8 manage.py migrate
STATUS=$?
TRY=2
while [ "$STATUS" != 0 ] && [ $TRY -gt 0 ]
do
    TRY=$((TRY-1))
    sleep 10
    python3.8 manage.py migrate
    STATUS=$?
done
popd
check_error $STATUS

WEEKLY_FILE="/flipstarterdirectorybackend/weekly.cron"
if [ -z "${FLIPSTARTER_REFRESH_WEEKLY}" ]
then
    echo "cron set to daily"
else
    touch ${WEEKLY_FILE}
    echo "cron set to weekly"
fi


daphne -b 0.0.0.0 -p 8001 flipstarterdirectorybackend.asgi:application
